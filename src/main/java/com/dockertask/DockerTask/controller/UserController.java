package com.dockertask.DockerTask.controller;

import com.dockertask.DockerTask.dto.UserDto;
import com.dockertask.DockerTask.model.User;
import com.dockertask.DockerTask.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/user")
@RestController()
@RequiredArgsConstructor
public class UserController {

    private final UserRepository userRepository;

    @PostMapping
    public User sayHello2(@RequestBody UserDto dto) {
        return  new User(1l,dto.getName(),dto.getSurname());

    }


}
