package com.dockertask.DockerTask.repository;

import com.dockertask.DockerTask.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository <User,Long>{
}
