package com.dockertask.DockerTask.dto;

import lombok.Data;

@Data
public class UserDto {
    private String name;
    private String surname;
}
